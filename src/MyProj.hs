module MyProj
    ( runMyProj
    ) where

runMyProj :: IO ()
runMyProj = putStrLn "This is a demo project."
